---
title: Pipelines and Languages
date: 2020-06-03
tags: ["pipelines", "language"]
---


So. google results:
*golang make a processing pipeline*  -> 0.6E6 results  
*Rust make a processing pipeline*    -> 3.7E6 results  
*clojure make a processing pipeline* -> 0.5E6 results  
*C++ make a processing pipeline*     -> 3.7E6 results  

I'm currently working with C++ because of work. The good'ol system programming language with a morbidly obese syntax.
It's like friggin' mike myers in a fat suit. 

Let me rant a bit about the results.

the Go search gives me a nice result on the first page, which is (https://blog.golang.org/pipelines). With some nice instructions on what the language itself can do without writing specially made functions. I like what I see.

The Rust search gives me a blog post inspired by the golang blogpost. [medium.com](https://medium.com/@polyglot_factotum/rust-concurrency-patterns-natural-born-pipelines-4d599e7612fc) Also not bad. Even though I at the time of writing don't know Rust, at all. The whole code example is less than 100 lines of code, and quite readable.

The Clojure search, as clojure is the least most used language of the ones I searched, is a bit sparse, but with some good uses of idiomatic Clojure, with transducers and multicore processing. One example [grammarly](https://www.grammarly.com/blog/engineering/building-etl-pipelines-with-clojure-and-transducers/)
(Transducers are composable algorithmic transformations), one of my favourite abstractions. It is a kind of generalization of the functional reduce, also called fold, but the wikipedia article for fold is HORRIBLE! Called 'accumulate' in the C++ `<numeric>` header. Or in C++17, the mystical three dots!  left fold is `(init ... op ... pack)` and right fold is ``(pack ... op ... init)`` where `pack` is an unpacked parameter pack, but ONLY for packed parameters. So it is no good for collections!. In Rust it is iterator.fold(initval, func)). But, I'm happy with the example, it gives me stuff to build on.

The C++ search results gives me... Research articles. On frameworks, for how to build pipelines. 
A second search gives me some nice blogs on functional pipelines (http://vitiy.info/functional-pipeline-in-c11/) and (https://helloacm.com/how-to-pipeline-the-functions-in-c/) which riff on the same ideas...
Maybe I'm searching the wrong terms? But why does the search give me relevant results in ALL THE OTHER LANGUAGES?
Now, there are MOUNDS of libraries doing this, including Boost. But despite the huge syntax of C++, nothing that is up to the task (quickly) from the main language. When I think of it it is actually in line with the philosophy of the language. 
All default IO provisions in C++ is just a context for managing  String/bytes/characters-like data (the universal interface is text, remember?). Which makes me puke. Passing the text output of a program into another and executing on it is a security hole. period. 
Turns out that if you want to use fast IOstream stuff, you gotta write the friggin locks yourself.

Please note that I have a personal distaste for including huge libraries when you only need that ONE thing in them. Which is why I think Boost is really, really, strange. Because they've got EVERYTHING. 

To conclude. If I want to do some stream-like processing of my data, I guess I have to write the mechanism of said stream by hand. 

So someone, please write a nice OS+kernel in Rust! so we can leave behind the vestiges of C++ in, maybe 50 years.

rant over? for now.
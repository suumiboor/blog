---
title: System modelling
date: 2020-04-23
---

*What kind of systems are we talking about?*

- Systems that are a composition of firmware/hardware/software

*ok?*

- Say we want to model a custom hardware, what do we do?

*we model in verilog, VHDL or some HDL language of our choice*

- Ok. Then what?

*we elaborate the design, and load it on an FPGA*

- Then we test it, find a bug and reiterate...
- And after we are convinced beyond reasonable doubt, we ship the system.

*yes!*

- OK. That is the "simple" design. What about this: You have 8 big FPGAs, that send data to an aggregator. Suddenly your computer cannot simulate the system in a reasonable amount of time. There is just too many clock cycles. How do we proceed?

*I dunno?*

- We **model** the system properly.

Modelling for systems is just one of those things that I've not been exposed to enough. But hey, I gotta learn somehow.

Basic systems consist of components, interconnected by some kind of communication protocol.
For analog circuits, the protocol is nodes and voltages/currents. For a network, it can be cpus and network sockets. For a microservice based application, it can be containers and grpc. For digital hardware, it is modules and busses.

Virtual platforms have the capability to run operating systems on them, while all we learn at university is just the barest minimum of what is possible. 
There are some tools available, the most standardized non-HDL of them is SystemC. But most languages have a Discrete event simulator (DES) library. 
Personally, I prefer higher-level languages. So I'm unfortunately biased against C++ just because of its immense lexicon. The large amount of ways to do things guarantee that it is more hard to make things simple.